<?PHP
$project_name = 'fukutabi';																//案件名
$root = '/';																						//ルート階層にアップする場合（本番環境用）
$root_arr = [
	'localhost'=>'http://localhost/'.$project_name.'/',								//localhostにアップする場合
	'localhost/MURATA' => 'http://localhost/MURATA/'.$project_name.'/_site/',
	'sarustar'=>'https://xampp.sarustar.net/MURATA/'.$project_name.'/_site/'	//sarustar.netにアップする場合
];
$current_url = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
foreach($root_arr as $host => $url){
	if(strpos($current_url,$host) !== false){ $root = $url; };
};
?>
<!doctype html>
<html lang="ja">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-35S0X48JNY"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-35S0X48JNY');
</script>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?=$title;?></title>
	<meta name="description" content="<?=$description;?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="canonical" href="https://fukutabi.info">
	<link rel="stylesheet" href="<?=$root;?>assets/css/base.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css">
	<link rel="stylesheet" href="<?=$root;?>assets/css/style.css">
</head>

<body>
	<header>
		<div class="container">
			<ul class="gnav d-none d-lg-block">
				<li><a href="<?=$root;?>#about">福旅とは</a></li>
				<li><a href="<?=$root;?>route_1.php">福旅ルート01</a></li>
				<li><a href="<?=$root;?>route_2.php">福旅ルート02</a></li>
				<li><a href="<?=$root;?>#coupon">クーポンの説明</a></li>
				<li><a href="<?=$root;?>routemap.php">路線図</a></li>
			</ul>
			<div class="d-block d-lg-none">
				<?PHP include("hamburger.php");?>
			</div>
		</div>
		<div class="drawer">
			<div class="drawer_inner">
				<div class="drawer_bg" style="background-image:url('assets/images/slogo.png')">
					<ul>
						<li><a href="<?=$root;?>">トップ</a></li>
						<li><a href="<?=$root;?>#about">福旅とは</a></li>
						<li><a href="<?=$root;?>route_1.php">福旅ルート01</a></li>
						<li><a href="<?=$root;?>route_2.php">福旅ルート02</a></li>
						<li><a href="<?=$root;?>#coupon">クーポンの説明</a></li>
						<li><a href="<?=$root;?>routemap.php">路線図</a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
