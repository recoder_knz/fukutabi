	<section class="contact">
		<div class="container px-4">
			<div class="content_box">
				<div class="title">
					<h2><span>お問い合わせ</span></h2>
				</div>
				<p class="text-md-center">福井・永平寺周遊滞在型観光推進委員会事務局<small>（福井市おもてなし観光推進課内）</small></p>
				<p class="text-md-center">adress. 〒910-0858 福井市手寄1丁目4-1　<br class="sp"
				>tel. 0776-20-5346（平日AM8:30〜PM5:15）</p>
			</div>
		</div>
	</section>
</main>
<footer>
	<div class="container">
		<p><small>&copy;2022 fukutabi</small></p>
	</div>
</footer>
<script src="<?=$root;?>assets/js/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollTrigger/1.0.5/ScrollTrigger.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>
<script src="<?=$root;?>assets/js/jquery.rwdImageMaps.min.js"></script>
<script src="<?=$root;?>assets/js/jquery.backstretch.min.js"></script>
<script src="<?=$root;?>assets/js/common.js"></script>
<script src="<?=$root;?>assets/js/drawer.js"></script>
</body>
</html>
