const trigger = new ScrollTrigger.default({
            once: true, // same functionality as the `once` flag in v0.x
            offset: {
                element: {
                    y: 1.0 // note that we pass a float instead of an integer, when the
                        // offset is a float, it takes it as a percentage, in this
                        // case, add 100% of the height of the element, the same
                        // functionality as the `addHeight` flag in v0.x
                }
            },
        });
        trigger.add('[data-scroll=animate__fadeIn]', {
            once: false,
            toggle: {
                class: {
                    in: 'fadeIn-2s',
                }
            }
        });
        trigger.add('[data-scroll=animate__fadeInUp]', {
            once: false,
            toggle: {
                class: {
                    in: 'fadeInUp-2s',
                }
            }
        });
        trigger.add('[data-scroll=animate__slideInDown]', {
            once: false,
            toggle: {
                class: {
                    in: 'slideInDown-2s',
                }
            }
        });

$(function(){
    $('a[href^="#"]').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});

$(function(){
  $('img[usemap]').rwdImageMaps();
});