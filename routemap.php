<?PHP
$title = '路線図｜福旅';
$description = '福旅は、「名勝 養浩館庭園」「大本山永平寺」「一乗谷朝倉氏遺跡」をマルッと周遊できる交通パスです。観光施設の入場券や、飲食店のクーポンもついた新提案。福井観光プランのコンテンツとしてぜひご活用ください。';
include("parts/header.php");
?>
<main id="main">
	<section class="mainvisual second">
		<div class="container">
			<div class="d-flex align-items-center justify-content-center">
				<h1 class="logo"><a href="/"><img src="assets/images/slogo.png" alt="福旅"></a></h1>
			</div>
		</div>
	</section>
	<section class="routemap">
		<div class="container px-4">
			<div class="content_box">
				<article class="content">
					<div class="title">
						<h2 class="map1"><span>TRAIN&amp;BUS ROUTE MAP</span><br></h2>
					</div>
					<div class="routemap_img">
						<img src="assets/images/routemap.png" alt="">
					</div>
				</article>
				<article class="content">
					<div class="title">
						<h2 class="map2"><span>TIMETABLE</span><br></h2>
					</div>
					<div class="alart">
						<p class="text-center">※2023年1月時点での情報です。<br>
						事前に各交通事業者のＨＰをご確認ください。</p>
					</div>
					<div class="timetable">
						<div class="table_block echizen">
							<div class="title">
								<h2>えちぜん鉄道</h2>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th scope="row" nowrap></th>
											<td nowrap></td>
											<td nowrap></td>
											<td nowrap></td>
											<td nowrap></td>
											<td colspan="2" nowrap>10時〜14時</td>
											<td nowrap></td>
											<td nowrap></td>
											<td colspan="2" nowrap>16時〜19時</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row" nowrap>福井駅</th>
											<td nowrap>8:28</td>
											<td nowrap>8:57</td>
											<td nowrap>9:25</td>
											<td nowrap>9:55</td>
											<td class="closeup" nowrap>毎時25</td>
											<td class="closeup" nowrap>毎時55</td>
											<td nowrap>15:25</td>
											<td nowrap>15:55</td>
											<td class="closeup" nowrap>毎時25</td>
											<td class="closeup" nowrap>毎時55</td>
										</tr>
										<tr>
											<th scope="row" nowrap>永平寺口駅</th>
											<td nowrap>8:55</td>
											<td nowrap>9:20</td>
											<td nowrap>9:50</td>
											<td nowrap>10:20</td>
											<td class="closeup" nowrap>毎時50</td>
											<td class="closeup" nowrap>毎時20</td>
											<td nowrap>15:50</td>
											<td nowrap>16:20</td>
											<td class="closeup" nowrap>毎時50</td>
											<td class="closeup" nowrap>毎時20</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th scope="row" nowrap></th>
											<td nowrap></td>
											<td nowrap></td>
											<td nowrap></td>
											<td nowrap></td>
											<td colspan="2" nowrap>10時〜15時</td>
											<td nowrap></td>
											<td nowrap></td>
											<td colspan="2" nowrap>17時〜19時</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row" nowrap>永平寺口駅</th>
											<td nowrap>8:33</td>
											<td nowrap>9:20</td>
											<td nowrap>9:25</td>
											<td nowrap>9:55</td>
											<td class="closeup" nowrap>毎時25</td>
											<td class="closeup" nowrap>毎時55</td>
											<td nowrap>15:25</td>
											<td nowrap>15:55</td>
											<td class="closeup" nowrap>毎時25</td>
											<td class="closeup" nowrap>毎時55</td>
										</tr>
										<tr>
											<th scope="row" nowrap>福井駅</th>
											<td nowrap>8:55</td>
											<td nowrap>9:20</td>
											<td nowrap>9:50</td>
											<td nowrap>10:20</td>
											<td class="closeup" nowrap>毎時50</td>
											<td class="closeup" nowrap>毎時20</td>
											<td nowrap>15:50</td>
											<td nowrap>16:20</td>
											<td class="closeup" nowrap>毎時50</td>
											<td class="closeup" nowrap>毎時20</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="table_block eiheiji">
							<div class="title">
								<h2>永平寺ライナー</h2>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>福井駅東口</th>
											<td nowrap>10:00</td>
											<td nowrap>10:50</td>
											<td nowrap>11:50</td>
											<td nowrap>12:55</td>
											<td nowrap>13:50</td>
											<td nowrap>14:50</td>
										</tr>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td nowrap>10:28</td>
											<td nowrap>11:18</td>
											<td nowrap>12:18</td>
											<td nowrap>13:23</td>
											<td nowrap>14:18</td>
											<td nowrap>15:18</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td nowrap>10:40</td>
											<td nowrap>11:30</td>
											<td nowrap>12:30</td>
											<td nowrap>13:30</td>
											<td nowrap>14:30</td>
											<td nowrap>15:30</td>
											<td nowrap>16:20</td>
										</tr>
										<tr>
											<th scope="row" nowrap>福井駅東口</th>
											<td nowrap>11:09</td>
											<td nowrap>11:59</td>
											<td nowrap>12:59</td>
											<td nowrap>13:59</td>
											<td nowrap>14:59</td>
											<td nowrap>15:59</td>
											<td nowrap>16:49</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="table_block ichijou">
							<div class="title">
								<h2>一乗谷永平寺連絡バス（京福バス）</h2>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>復原町並</th>
											<td nowrap>10:15</td>
											<td nowrap>13:15</td>
											<td nowrap>14:15</td>
										</tr>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td nowrap>10:32</td>
											<td nowrap>13:32</td>
											<td nowrap>14:32</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td nowrap>10:40</td>
											<td nowrap>13:40</td>
											<td nowrap>14:40</td>
										</tr>
										<tr>
											<th scope="row" nowrap>復原町並</th>
											<td nowrap>10:57</td>
											<td nowrap>13:57</td>
											<td nowrap>14:57</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="table_block keifuku">
							<div class="title">
								<h2>京福バス永平寺線・芦原丸岡永平寺線</h2>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>永平寺口</th>
											<td class="closeup1" rowspan="2">平日</td>
											<td nowrap>9:45</td>
											<td nowrap>10:15</td>
											<td nowrap>10:40</td>
											<td nowrap>11:25</td>
											<td nowrap>12:55</td>
											<td nowrap>13:25</td>
											<td nowrap>14:55</td>
											<td nowrap>15:25</td>
											<td nowrap>16:25</td>
											<td nowrap>17:55</td>
										</tr>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td nowrap>10:04</td>
											<td nowrap>10:28</td>
											<td nowrap>10:59</td>
											<td nowrap>11:38</td>
											<td nowrap>13:14</td>
											<td nowrap>13:38</td>
											<td nowrap>15:14</td>
											<td nowrap>15:38</td>
											<td nowrap>16:44</td>
											<td nowrap>18:14</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>永平寺口</th>
											<td class="closeup2" rowspan="2">土日祝</td>
											<td class="closeup1" nowrap>8:45</td>
											<td class="closeup1" nowrap>9:40</td>
											<td class="closeup1" nowrap>10:15</td>
											<td class="closeup1" nowrap>10:40</td>
											<td class="closeup1" nowrap>11:25</td>
											<td class="closeup1" nowrap>12:25</td>
											<td class="closeup1" nowrap>12:55</td>
											<td class="closeup1" nowrap>13:55</td>
											<td class="closeup1" nowrap>14:25</td>
											<td class="closeup1" nowrap>14:55</td>
											<td class="closeup1" nowrap>16:03</td>
											<td class="closeup1" nowrap>17:00</td>
										</tr>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td class="closeup1" nowrap>8:59</td>
											<td class="closeup1" nowrap>9:54</td>
											<td class="closeup1" nowrap>10:28</td>
											<td class="closeup1" nowrap>10:54</td>
											<td class="closeup1" nowrap>11:38</td>
											<td class="closeup1" nowrap>12:38</td>
											<td class="closeup1" nowrap>13:09</td>
											<td class="closeup1" nowrap>14:09</td>
											<td class="closeup1" nowrap>14:38</td>
											<td class="closeup1" nowrap>15:09</td>
											<td class="closeup1" nowrap>16:17</td>
											<td class="closeup1" nowrap>17:14</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td class="closeup1" rowspan="2">平日</td>
											<td nowrap>10:11</td>
											<td nowrap>11:41</td>
											<td nowrap>12:11</td>
											<td nowrap>13:41</td>
											<td nowrap>14:26</td>
											<td nowrap>15:19</td>
											<td nowrap>15:41</td>
											<td nowrap>16:56</td>
											<td nowrap>18:19</td>
										</tr>
										<tr>
											<th scope="row" nowrap>永平寺口</th>
											<td nowrap>10:30</td>
											<td nowrap>11:56</td>
											<td nowrap>12:30</td>
											<td nowrap>13:56</td>
											<td nowrap>14:45</td>
											<td nowrap>15:38</td>
											<td nowrap>15:56</td>
											<td nowrap>17:15</td>
											<td nowrap>18:38</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>永平寺</th>
											<td class="closeup2" rowspan="2">土日祝</td>
											<td class="closeup1" nowrap>9:11</td>
											<td class="closeup1" nowrap>10:11</td>
											<td class="closeup1" nowrap>11:41</td>
											<td class="closeup1" nowrap>12:11</td>
											<td class="closeup1" nowrap>13:23</td>
											<td class="closeup1" nowrap>13:41</td>
											<td class="closeup1" nowrap>14:19</td>
											<td class="closeup1" nowrap>14:41</td>
											<td class="closeup1" nowrap>15:19</td>
											<td class="closeup1" nowrap>15:41</td>
											<td class="closeup1" nowrap>16:30</td>
											<td class="closeup1" nowrap>17:50</td>
										</tr>
										<tr>
											<th scope="row" nowrap>永平寺口</th>
											<td class="closeup1" nowrap>9:25</td>
											<td class="closeup1" nowrap>10:25</td>
											<td class="closeup1" nowrap>11:56</td>
											<td class="closeup1" nowrap>12:25</td>
											<td class="closeup1" nowrap>13:37</td>
											<td class="closeup1" nowrap>13:56</td>
											<td class="closeup1" nowrap>14:33</td>
											<td class="closeup1" nowrap>14:56</td>
											<td class="closeup1" nowrap>15:33</td>
											<td class="closeup1" nowrap>15:56</td>
											<td class="closeup1" nowrap>16:44</td>
											<td class="closeup1" nowrap>18:04</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="table_block ichijou">
							<div class="title">
								<h2>京福バス一乗谷東郷線</h2>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>福井駅</th>
											<td nowrap>8:00</td>
											<td nowrap>9:20</td>
											<td nowrap>10:40</td>
											<td nowrap>12:00</td>
											<td nowrap>13:40</td>
											<td nowrap>15:00</td>
										</tr>
										<tr>
											<th scope="row" nowrap>復原町並</th>
											<td nowrap>8:28</td>
											<td nowrap>9:48</td>
											<td nowrap>11:08</td>
											<td nowrap>12:28</td>
											<td nowrap>14:08</td>
											<td nowrap>15:28</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th scope="row" nowrap>復原町並</th>
											<td nowrap>10:01</td>
											<td nowrap>11:21</td>
											<td nowrap>12:46</td>
											<td nowrap>14:21</td>
											<td nowrap>15:41</td>
											<td nowrap>18:31</td>
										</tr>
										<tr>
											<th scope="row" nowrap>福井駅</th>
											<td nowrap>10:30</td>
											<td nowrap>11:50</td>
											<td nowrap>13:15</td>
											<td nowrap>14:50</td>
											<td nowrap>16:10</td>
											<td nowrap>19:00</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>	
				</article>
			</div>
		</div>
	</section>
<?PHP include("parts/footer.php"); ?>
