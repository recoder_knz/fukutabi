<?PHP
$title = '福旅ルート02｜福旅';
$description = '福旅は、「名勝 養浩館庭園」「大本山永平寺」「一乗谷朝倉氏遺跡」をマルッと周遊できる交通パスです。観光施設の入場券や、飲食店のクーポンもついた新提案。福井観光プランのコンテンツとしてぜひご活用ください。';
include("parts/header.php");
?>
<main id="main">
	<section class="mainvisual second">
		<div class="container">
			<div class="d-flex align-items-center justify-content-center">
				<h1 class="logo"><a href="/"><img src="assets/images/slogo.png" alt="福旅"></a></h1>
			</div>
		</div>
	</section>
	<section class="route">
		<div class="container px-4">
			<div class="content_box">
				<article class="head">
					<div class="row g-5 align-items-center justify-content-center">
						<div class="col-lg-4 route_logo">
							<img src="assets/images/route_title2.png" alt="">
						</div>
						<div class="col-lg-8">
							<h2>東尋坊・恐竜博物館を巡る、<br>
							福井定番コース</h2>
							<p>福旅ルート02は、福井の誇る人気観光地を大満喫するモデルルートです。DAY1では全国的に有名な景勝地「東尋坊」、DAY2では世界的評価も高い「福井県立恐竜博物館」へ、DAY3では福井・永平寺エリアパスを使って福井の歴史的建造物に足を運びます。<br>
							（※福井・永平寺エリアパスが使える箇所はDAY3のみとなります）</p>
						</div>
					</div>
				</article>
				<article class="content">
					<div class="route_title">
						<h2>DAY1.2<br>
						福井の名所を巡る</h2>
					</div>
					<div class="row g-5 justfiy-content-center">
						<div class="col-md-6 route_scd">
							<img src="assets/images/route_2_1.png" alt="">
						</div>
						<div class="col-md-6">
							<div class="route_content">
								<h3>大迫力の断崖絶壁「東尋坊」</h3>
								<p>波の浸食によって荒々しくカットされた岸壁が、三国町の海岸線約1kmにわたり続く「東尋坊」。国の天然記念物に指定されており、高さ55mの東尋坊タワーや、周辺をめぐる遊覧船からは美しい景観が楽しめます。</p>
								<div class="route_content_img">
									<div class="route_img_one">
										<img src="assets/images/route2_img1_1.jpg" alt="">
										<p>東尋坊</p>
									</div>
								</div>
							</div>
							<div class="route_content">
								<h3>「福井県立恐竜博物館」でたのしく、学ぶ</h3>
								<p>「福井県立恐竜博物館」は、恐竜を中心とする地質古生物学専門の博物館です。4,500平米という広大な展示室に、千数百点もの標本、大型復元ジオラマや映像が設置されています。</p>
								<div class="route_content_img">
									<div class="route_img_one">
										<img src="assets/images/route2_img1_2.jpg" alt="">
										<p>福井県立恐竜博物館</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
				<article class="content">
					<div class="route_title">
						<h2 class="pass">DAY3<br>
						もう一足伸ばそう</h2>
					</div>
					<div class="row g-5 justfiy-content-center">
						<div class="col-md-6 route_scd">
							<img src="assets/images/route_2_2.png" alt="">
							<p>※1　そばクーポンはどこかの店舗で一度しかご使用できません。<br>
							※2　お土産クーポンはいずれかの観光地で一度しかご使用できません。</p>
						</div>
						<div class="col-md-6">
							<div class="route_content">
								<h3>歴史の余韻に浸りながら<br>
								「一乗谷レストラント」へ</h3>
								<p>「一乗谷朝倉氏遺跡」を訪れたあとは、隣接するレストランでのランチはいかがでしょうか。素敵なロケーションの中で、おろしそばや、旬のお料理のランチセットをぜひご堪能下さい。</p>
								<div class="route_content_img">
									<div class="route_img_one">
										<img src="assets/images/route2_img2_1.jpg" alt="">
										<p>一乗谷レストラント</p>
									</div>
								</div>
							</div>
							<div class="route_content">
								<h3>福井旅行のラストは、<br>
								駅前で「シメのおそば」</h3>
								<p>大根おろし出汁と十割そばの絶妙な相性を楽しめる「あみだそば 福の井」さん、県産そば粉、天然水、厳選素材の出汁を使用し、味わいや香りにこだわったおそばを提供する「越前蕎麦倶楽部」さん、通常の手打ちそばのほか、昔ながらの家庭での手打ちそばをイメージした超極太麺も食べられる「福福茶屋」さん。名店のおそばで、福井旅行を締めくくりませんか？</p>
								<div class="route_content_img">
									<div class="row g-4">
										<div class="col-md-6 route_img_one soba">
											<img src="assets/images/route2_img2_2.jpg" alt="">
											<p>福福茶屋</p>
										</div>
										<div class="col-md-6 route_img_one soba">
											<img src="assets/images/route2_img2_3.jpg" alt="">
											<p>越前蕎麦倶楽部</p>
										</div>
										<div class="col-md-6 route_img_one soba">
											<img src="assets/images/route2_img2_4.jpg" alt="">
											<p>あみだそば 福の井</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>
<?PHP include("parts/footer.php"); ?>
