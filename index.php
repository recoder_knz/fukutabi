<?PHP
$title = '福旅｜「名勝 養浩館庭園」「大本山永平寺」「一乗谷朝倉氏遺跡」をマルッと周遊できる交通パス';
$description = '福旅は、「名勝 養浩館庭園」「大本山永平寺」「一乗谷朝倉氏遺跡」をマルッと周遊できる交通パスです。観光施設の入場券や、飲食店のクーポンもついた新提案。福井観光プランのコンテンツとしてぜひご活用ください。';
include("parts/header.php");
?>
<main id="main">
	<section class="mainvisual">
		<div class="container vh100">
			<div class="d-flex align-items-center justify-content-center vh100">
				<h1 class="logo"><a href="<?=$root;?>"><img src="assets/images/logo.png" alt="福旅｜「名勝 養浩館庭園」「大本山永平寺」「一乗谷朝倉氏遺跡」をマルッと周遊できる交通パス"></a></h1>
			</div>
		</div>
	</section>
	<section id="about" class="about">
		<div class="container px-4">
			<div class="content_box">
				<article class="content">
					<div class="title">
						<h2><span>福旅とは</span></h2>
					</div>
					<div class="row justify-content-center">
						<div class="col-lg-7">
							<p>福旅は、「名勝 養浩館庭園」「大本山永平寺」「一乗谷朝倉氏遺跡」をマルッと周遊できる交通パスです。<br>
							観光施設の入場券や、飲食店のクーポンもついた新提案。<br>
							福井観光プランのコンテンツとしてぜひご活用ください。</p>
						</div>
					</div>
					<div class="otoku_img">
						<img src="assets/images/otoku_img.png" alt="">
					</div>
					<div class="otoku_img">
						<img src="assets/images/guidebook_img.jpg" alt="">
					</div>
				</article>
				<article class="content">
					<div class="title">
						<h2><span>FUKUI AREA MAP</span></h2>
					</div>
					<div class="row justify-content-center mb-4">
						<div class="col-lg-7">
							<p class="text-center">マップ内のⒶⒷⒸをクリックすると、詳細情報が表示します。</p>
						</div>
					</div>
					<div class="map_pc d-none d-md-block">
						<img src="assets/images/map_pc.png" usemap="#image-map1" alt="">
						<map name="image-map1">
							 <area target="" alt="" title="" href="assets/images/map_detail1.png" coords="420,353,156,164" shape="rect" data-lightbox="pc-group">
							 <area target="" alt="" title="" href="assets/images/map_detail2.png" coords="1594,616,1233,365" shape="rect" data-lightbox="pc-group">
							 <area target="" alt="" title="" href="assets/images/map_detail3.png" coords="846,906,1139,1094" shape="rect" data-lightbox="pc-group">
						</map>
					</div>
					<div class="map_sp d-block d-md-none">
						<img src="assets/images/map_sp1.png" usemap="#ImageMap2" alt="" />
						<map name="ImageMap2">
							<area shape="rect" coords="46,39,248,201" href="assets/images/map_detail1.png" data-lightbox="sp-group" alt="" />
							<area shape="rect" coords="560,183,879,369" href="assets/images/map_detail2.png" data-lightbox="sp-group" alt="" />
							<area shape="rect" coords="358,467,632,622" href="assets/images/map_detail3.png" data-lightbox="sp-group" alt="" />
						</map>
						<img src="assets/images/map_sp2.png" alt="">
					</div>
				</article>
			</div>
		</div>
	</section>
	<section id="coupon" class="coupon">
		<div class="container px-4">
			<div class="content_box">
				<div class="title">
					<h2><span>福旅クーポンのご説明</span></h2>
				</div>
				<article class="content">
					<p class="text-md-center">福旅クーポンは全3種類。<br>
					「大本山永平寺拝観クーポン」、ご紹介したお店で使える「おそば割引クーポン」、<br class="pc"
					>旅先でノベルティと交換できる「お土産クーポン」です。</p>
				</article>
				<article class="content">
					<div class="row coupon_one g-3">
						<div class="col-md-8">
							<h2>大本山永平寺拝観クーポン</h2>
							<p>ご入場時に、境内の総受処にて添付のクーポンを切り取ってお渡しいただき、ご入場手続きを行ってください。<br>
							<span class="red">※お1人様の使用に限ります。<br>
							※一度しかご利用できません。</span></p>
						</div>
						<div class="col-md-4">
							<img src="assets/images/coupon_img1.jpg" alt="">
						</div>
					</div>
					<div class="row coupon_one g-3">
						<div class="col-md-8">
							<h2>おそば割引クーポン</h2>
							<p>福井県産そば粉を使ったおそばが、各店で割引になるお得なクーポンチケットです。対象店舗は、「あぜ川」、「山楽亭」、「てらぐち」、「あみだそば 福の井」、「越前蕎麦倶楽部」、「福福茶屋」「一乗谷レストラント」（計7店舗）です。ご注文時に店員の方へ添付のクーポンをお渡しください。<br>
							<span class="red">※お1人様の使用に限ります。<br>
							※そばクーポンはどこかの店舗で一度しかご使用できません。</span></p>
						</div>
						<div class="col-md-4">
							<img src="assets/images/coupon_img2.jpg" alt="">
						</div>
					</div>
					<div class="coupon_one">
						<h2>お土産クーポン</h2>
						<p class="mb-4">対象の施設は、「大本山永平寺」「名勝 養浩館庭園」「一乗谷朝倉氏遺跡」です。各施設の窓口にて添付のクーポンをお渡しいただき、お土産とお引換えください。（永平寺はP10①〜③のおそば割引クーポン対象店での引換えになります。）<br>
						<span class="red">※お1人様の使用に限ります。<br>
						※お土産クーポンはどこかの観光地で一度しかご使用できません。</span></p>
						<div class="row g-3">
							<div class="coupon_img_one col-md-6 col-lg-4">
								<img src="assets/images/coupon_img3_1.jpg" alt="">
								<p>大本山永平寺：エコバッグ（1枚）</p>
							</div>
							<div class="coupon_img_one col-md-6 col-lg-4">
								<img src="assets/images/coupon_img3_2.jpg" alt="">
								<p>名勝 養浩館庭園：ポストカード（1枚）</p>
							</div>
							<div class="coupon_img_one col-md-6 col-lg-4">
								<img src="assets/images/coupon_img3_3.jpg" alt="">
								<p>一乗谷朝倉氏遺跡：オリジナルマグネット（1個）</p>
							</div>
						</div>
					</div>
					<div class="coupon_one">
						<div class="row g-5">
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img1.jpg" alt="あぜ川">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❶</span>あぜ川</h3>
										<p>約90年続く秘伝の出汁と、<br>
										平打ちの早刈りそば。</p>
										<p><small>福井県吉田郡永平寺町志比24-41<br>
										tel. 0776-63-3450<br>
										open.11:00～15:00(LO.14:30)<br>
										土日祝.9:30～16:00(LO.15:20)<br>
										定休日.水曜、木曜</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img2.jpg" alt="山楽亭">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❷</span>山楽亭</h3>
										<p>伝統的な手打ちそば、<br>
										イチおしは「おろし蕎麦」。</p>
										<p><small>福井県吉田郡永平寺町志比23-31-10<br>
										tel.0776-63-3023<br>
										open.10:00～16:00<br>
										GW・お盆.9:00～17:00<br>
										定休日.金曜(4～11月)<br>
										不定休.(12～3月)</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img3.jpg" alt="てらぐち">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❸</span>てらぐち</h3>
										<p>永平寺門前すぐ、<br>
										県産そば粉が楽しめる。</p>
										<p><small>福井県吉田郡永平寺町志比5-17<br>
										tel.0776-63-3064<br>
										open.9:00～16:00(LO.15:30)<br>
										定休日.火曜</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img4.jpg" alt="福福茶屋">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❹</span>福福茶屋</h3>
										<p>福井の昔ながらのそば<br>
										「超極太麺」も楽しめる。</p>
										<p><small>福井県福井市中央1-2-1ハピリン1F<br>
										tel.0776-20-2929<br>
										open.11:00～22:00（L.O.21:00）<br>
										定休日.年中無休</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img5.jpg" alt="あみだそば 福の井">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❺</span>あみだそば 福の井</h3>
										<p>三種の出汁と十割そばの<br>
										絶妙な相性が楽しめる。</p>
										<p><small>福井県福井市中央1-2-1ハピリン1F<br>
										tel.0776-43-0739<br>
										open.10:30～20:30（L.O.20:00）<br>
										定休日.年中無休</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img6.jpg" alt="越前蕎麦倶楽部">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❻</span>越前蕎麦倶楽部</h3>
										<p>味わい、香りが際立つ<br>
										「あげおろしそば」がイチおし。</p>
										<p><small>福井県福井市中央1-10-21<br>
										tel.050-3637-3101<br>
										open.11:00～14:00<br>
										定休日.火曜</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img7.jpg" alt="一乗谷レストラント">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❼</span>一乗谷レストラント</h3>
										<p>素敵なロケーションで、<br>
										本格手打ちそばを堪能。</p>
										<p><small>福井県福井市城戸ノ内町10-48<br>
										tel.0776-97-5335<br>
										open.11:00～15:00(LO.14:30)<br>
										定休日.火曜、水曜<br>
										(予約状況により変更する場合があります)</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img8.jpg" alt="大本山永平寺">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❽</span>大本山永平寺</h3>
										<p>曹洞宗の開祖・道元禅師が<br>
										1244年に開いた修行道場。</p>
										<p><small>福井県吉田郡永平寺町志比5-15<br>
										tel.0776-63-3102<br>
										open.8:30～17:00<br>
										※入場は16:30まで<br>
										定休日.無休</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img9.jpg" alt="名勝 養浩館庭園">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">❾</span>名勝 養浩館庭園</h3>
										<p>福井藩主松平家の別邸。<br>
										特に秋の紅葉が魅力。</p>
										<p><small>福井県福井市宝永3-11-36<br>
										tel.0776-21-0489<br>
										open.9:00～19:00<br>
										(11/6～2月末は17:00閉園)<br>
										※入園は閉園30分前まで<br>
										定休日.年末年始</small></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 coupon_shop">
								<div class="row g-4">
									<div class="col-5 col-lg-6">
										<img src="assets/images/shop_img10.jpg" alt="一乗谷朝倉氏遺跡">
									</div>
									<div class="col-7 col-lg-6">
										<h3><span class="red">➓</span>一乗谷朝倉氏遺跡</h3>
										<p>400年の時を経て蘇った<br>
										中世最大の戦国城下町跡。</p>
										<p><small>福井県福井市城戸ノ内町28-37<br>
										tel.0776-41-2330<br>
										open.9:00～17:00<br>
										※復原町並入場は16:30まで<br>
										定休日.年末年始</small></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>
	<section class="ticket">
		<div class="container px-4">
			<div class="content_box">
				<div class="title">
					<h2><span>購入できる場所</span></h2>
				</div>
				<p class="text-md-center">京福バスチケットセンター(えちぜん鉄道福井駅1階)</p>
				<p class="text-md-center"><small>adress.〒910-0006 福井市中央1丁目1番1号3<br>
				tel.0776-57-7700</small></p>
			</div>
		</div>
	</section>
	<section class="attention">
		<div class="container px-4">
			<div class="content_box">
				<div class="title">
					<h2><span>取扱事項</span></h2>
				</div>
				<ul>
					<li><big>1冊2,400円（税込）です。</big></li>
					<li>日付記載日のみ有効です。</li>
					<li>お一人様の使用に限ります。</li>
					<li>電車やバスは日付記載日に限り何回でも乗り降りできます。</li>
					<li>えちぜん鉄道「勝山永平寺線福井駅から永平寺口駅の区間」にて有効です。</li>
					<li>京福バス「一乗谷永平寺連絡バス」、「永平寺ライナー」、「一乗谷東郷線」の福井駅から復原町並、「永平寺線」「芦原丸岡永平寺線」の永平寺口から永平寺にて有効です。</li>
					<li>購入後の払い戻しは致しません。</li>
					<li>三国花火大会当日は、ご利用いただけません。</li>
				</ul>
			</div>
		</div>
	</section>
<?PHP include("parts/footer.php"); ?>
