<?PHP
$title = '福旅ルート01｜福旅';
$description = '福旅は、「名勝 養浩館庭園」「大本山永平寺」「一乗谷朝倉氏遺跡」をマルッと周遊できる交通パスです。観光施設の入場券や、飲食店のクーポンもついた新提案。福井観光プランのコンテンツとしてぜひご活用ください。';
include("parts/header.php");
?>
<main id="main">
	<section class="mainvisual second">
		<div class="container">
			<div class="d-flex align-items-center justify-content-center">
				<h1 class="logo"><a href="/"><img src="assets/images/slogo.png" alt="福旅"></a></h1>
			</div>
		</div>
	</section>
	<section class="route">
		<div class="container px-4">
			<div class="content_box">
				<article class="head">
					<div class="row g-5 align-items-center justify-content-center">
						<div class="col-lg-4 route_logo">
							<img src="assets/images/route_title1.png" alt="">
						</div>
						<div class="col-lg-8">
							<h2>福井の歴史と酒蔵を<br>
							堪能する福井満喫コース</h2>
							<p>福旅ルート01では、福井の誇る歴史的建造物を巡りながら、豊かな自然とおいしい水に育まれた名酒と出会うモデルルートをご紹介。DAY1では、大本山永平寺や一乗谷朝倉氏遺跡、名勝 養浩館庭園を訪れます。DAY2ではさらにもう一足伸ばし、白山平泉寺や大野、永平寺の酒蔵を回ります。（※福井・永平寺エリアパスが使える箇所はDAY1のみとなります）</p>
						</div>
					</div>
				</article>
				<article class="content">
					<div class="route_title">
						<h2 class="pass">DAY1<br>
						福井・永平寺エリアパスを使って巡る</h2>
					</div>
					<div class="row g-5 justfiy-content-center">
						<div class="col-md-6 route_scd">
							<img src="assets/images/route_1_1.png" alt="">
							<p>※1　そばクーポンはどこかの店舗で一度しかご使用できません。<br>
							※2　お土産クーポンはいずれかの観光地で一度しかご使用できません。</p>
						</div>
						<div class="col-md-6">
							<div class="route_content">
								<h3>お昼は永平寺で「おそばランチ」。</h3>
								<p>永平寺を訪れた後は、門前での「おそばランチ」がおすすめ！約90年続く秘伝の出汁でいただく「あぜ川」さん、辛味大根がよく利いた3代伝統の手打ちそば「山楽亭」さん、門前の目の前にありお食事後にはお土産さがしも楽しめる「てらぐち」さん。冊子内にある「おそば割引クーポン」で、福井のおそばをおトクに楽しめます。</p>
								<div class="route_content_img">
									<div class="route_img_one">
										<img src="assets/images/route1_img1_1.jpg" alt="">
										<p>山楽亭</p>
									</div>
									<div class="route_img_one">
										<img src="assets/images/route1_img1_2.jpg" alt="">
										<p>てらぐち</p>
									</div>
									<div class="route_img_one">
										<img src="assets/images/route1_img1_3.jpg" alt="">
										<p>あぜ川</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
				<article class="content">
					<div class="route_title">
						<h2>DAY2<br>
						もう一足伸ばそう</h2>
					</div>
					<div class="row g-5 justfiy-content-center">
						<div class="col-md-6 route_scd">
							<img src="assets/images/route_1_2.png" alt="">
						</div>
						<div class="col-md-6">
							<div class="route_content">
								<h3>酒蔵で出会う、福井の名酒と豊かな自然</h3>
								<p>酒づくりに欠かせないもの、それは良い水、良い米、そして豊かな経験を持つ蔵人です。福井は、まさに水・米・人の３拍子揃った最高の酒処といえます。福旅では、福井の誇るたくさんの酒蔵の中から永平寺町の「黒龍酒造」さん、「田辺酒造」さんをご紹介しています。</p>
								<div class="route_content_img">
									<div class="route_img_one">
										<img src="assets/images/route1_img2_1.jpg" alt="">
										<p>黒滝酒造</p>
									</div>
									<div class="route_img_one">
										<img src="assets/images/route1_img2_2.jpg" alt="">
										<p>田辺酒造</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</section>
<?PHP include("parts/footer.php"); ?>
